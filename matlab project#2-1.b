    %Gaussian elimination
    %b.return a row echelon form for any given input matrix
    function [Echelon_matrix]=my_echelonform(coeff_matrix)
    [row_coeff,col_coeff] = size(coeff_matrix);
    i = 1;
    j = 1;

    while i < (row_coeff+1)


    if all(coeff_matrix(:,j) == 0) == 1
        j = j+1;

    else     
        if coeff_matrix(i,j)~=0
            for k=i+1:row_coeff
                coeff_matrix(k,:)=coeff_matrix(k,:)-(coeff_matrix(k,j)/coeff_matrix(i,j))*coeff_matrix(i,:);
            end


        elseif coeff_matrix(i,j)==0
            for k=i+1:row_coeff
                if coeff_matrix(k,j)~=0
                    coeff_matrix([i,k],:)=coeff_matrix([k,i],:);
                    break
                end

            end
            for k=i+1:row_coeff
                coeff_matrix(k,:)=coeff_matrix(k,:)-(coeff_matrix(k,j)/coeff_matrix(i,j))*coeff_matrix(i,:);
            end
        end

        i = i+1;
        j = j+1;


    end

    end


    Echelon_matrix=coeff_matrix;

    end