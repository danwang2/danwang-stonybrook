%Gaussian elimination
%c.return a row canonical form for any given input row echelon matrix    
function [canonical_matrix]=my_canonicalform(coeff_matrix)
[row_coeff,col_coeff] = size(coeff_matrix);
check=testmatrix(coeff_matrix);

if check==0
    fprintf('error! Please input a row echelon form')
elseif check==1 
    for i=row_coeff:-1:1
        count=find(coeff_matrix(i,:)~=0);
        if count~=0
            for k1=1:col_coeff
                coeff_matrix(i,k1)=(coeff_matrix(i,k1))*(1/(coeff_matrix(i,count(1))));
            end
            for m=i-1:-1:1
                if coeff_matrix(m,count(1))~=0
                    for k2=1:col_coeff
                        coeff_matrix(m,k2)=(coeff_matrix(m,k2))-(coeff_matrix(m,count(1)))/(coeff_matrix(i,count(1)))*(coeff_matrix(i,k2));
                    end
                end
            end
        end
    end
    canonical_matrix=coeff_matrix;
end

end
