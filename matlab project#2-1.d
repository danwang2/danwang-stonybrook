%Gaussian elimination
%d.return the solution to Ax=b
function [x_matrix,time] = GaussElimination(coeff_matrix,vec_matrix)

t1=clock;
[row_coeff,col_coeff] = size(coeff_matrix);
[row_vec,~] = size(vec_matrix);

if (row_coeff ~= col_coeff)||(row_coeff ~= row_vec)    
    fprintf('error\n');
    return
else
    B=eye(row_coeff);
    A=[coeff_matrix,B];
    A_1=my_echelonform(A);
    A_2=my_canonicalform(A_1);
    A_3=A_2(1:(row_coeff),((row_coeff)+1):(2*(row_coeff)));
    A_4=A_2(1:row_coeff,1:row_coeff);
    if sum(abs(A_4-B))<1e-10
        x_matrix=(A_3).*(vec_matrix);
    else
        fprintf('no solution\n');
    end
end
t2=clock;
time=etime(t2,t1);
end
   