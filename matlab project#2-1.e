%Gaussian elimination
%e.test the function
function [result] = test_function(A1,B1)
result = zeros(1,10);
for i = 100:100:1000
    A1 =round(10*rand(i));
    B1 =1+round(10*rand(i,1));
    [~,time] = GaussElimination(A1,B1);
    j = i/100;
    result(1,j)=time;
end
end